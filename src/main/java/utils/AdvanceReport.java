package utils;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class AdvanceReport {
	static ExtentHtmlReporter html;
	static ExtentReports extent;
	public static ExtentTest test;

	public void startReport() {
		html = new ExtentHtmlReporter("./reports/extentReport.html");
		extent = new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
	}

	public void initializeTest(String tcName, String tcDescription, String tcAuthor, String tcCategory) {
		test = extent.createTest(tcName, tcDescription);
		test.assignAuthor(tcAuthor);
		test.assignCategory(tcCategory);
	}

	public void logStep(String tcDescription, String status) {
		if (status.equalsIgnoreCase("pass"))
			test.pass(tcDescription);
		else if (status.equalsIgnoreCase("fail"))
			test.fail(tcDescription);
		else if (status.equalsIgnoreCase("warning"))
			test.warning(tcDescription);
	}

	public void endReport() {
		extent.flush();
	}

}
