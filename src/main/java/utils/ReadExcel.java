package utils;

import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	public static Object[][] data;

	@SuppressWarnings("static-access")
	public static Object[][] readData(String datasheetsource) {
		// Enter the Excel Path
		XSSFWorkbook wbook;
		try {
			wbook = new XSSFWorkbook("./data/" + datasheetsource + ".xlsx");

			// Enter the Sheet Name
			XSSFSheet sheet = wbook.getSheet("Sheet1");
			// Get the rows count
			int lastRowNum = sheet.getLastRowNum();
			// Get the columns count
			short lastCellNum = sheet.getRow(0).getLastCellNum();
			data = new Object[lastRowNum][lastCellNum];
			for (int i = 1; i <= lastRowNum; i++) {
				XSSFRow row = sheet.getRow(i);
				for (int j = 0; j < lastCellNum; j++) {
					XSSFCell cell = row.getCell(j);
					CellType cellType = cell.getCellType();

					if (cellType.STRING != null)
						data[i - 1][j] = cell.getStringCellValue();
					else if (cellType.NUMERIC != null) {
						if (DateUtil.isCellDateFormatted(cell))
							data[i - 1][j] = cell.getDateCellValue();
						else
							data[i - 1][j] = cell.getNumericCellValue();
					} else if (cellType.BOOLEAN != null)
						data[i - 1][j] = cell.getBooleanCellValue();
					else if (cellType._NONE != null)
						data[i - 1][j] = null;
					else if (cellType.ERROR != null)
						data[i - 1][j] = cell.getErrorCellValue();
					else if (cellType.FORMULA != null)
						data[i - 1][j] = cell.getCellFormula();
				}
			}

		} catch (

		IOException e) {
			e.printStackTrace();
		}
//		wbook.close();
		return data;

	}

}
