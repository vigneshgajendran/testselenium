package bl.framework.commonMethods;

import java.util.concurrent.TimeUnit;

import javax.naming.ldap.StartTlsResponse;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import bl.framework.api.SeleniumBase;
import utils.ReadExcel;

public class ProjectMethods extends SeleniumBase {

	public String tcName, tcDescription, tcAuthor, tcCategory,datasheetsource;

	//@BeforeSuite(groups = { "common" })
	@BeforeSuite
	public void beforeSuite() {
		startReport();
	}

	//@BeforeClass(groups = { "common" })
	@BeforeClass
	public void beforeClass() {
		initializeTest(tcName, tcDescription, tcAuthor, tcCategory);
	}

	//@BeforeMethod(groups = { "common" }
	@Parameters({"url","username","password"})
	@BeforeMethod
	public void login(String url,String username,String password) {
		startApp("chrome", url);
		clearAndType(locateElement("id", "username"), username);
		clearAndType(locateElement("id", "password"), password);
		click(locateElement("class", "decorativeSubmit"));
		click(locateElement("link", "CRM/SFA"));
	}

	//@AfterMethod(groups = { "common" })
	@AfterMethod(enabled=false)
	public void closeApp() {
		close();
	}

	//@AfterSuite(groups = { "common" })
	@AfterSuite
	public void afterSuite() {
		endReport();
	}
	
	@DataProvider(name="getExcelData")
	public Object[][] getDataFromExcel() {
		return ReadExcel.readData(datasheetsource);
	}

}
