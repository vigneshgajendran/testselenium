package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.compress.compressors.snappy.SnappyCompressorInputStream;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import utils.AdvanceReport;

public class SeleniumBase extends AdvanceReport implements Browser, Element {

	public static RemoteWebDriver driver;
	public int i = 1;

	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--disable-notifications");				
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver(options);
			} else if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.manage().window().maximize();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			takeSnap();
			logStep("The browser " + browser + " launched successfully", "pass");
		} catch (WebDriverException e) {
			logStep("Unable to launch the " + browser + " browser", "fail");
		}
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {

		try {
			switch (locatorType) {
			case "id":
				return driver.findElementById(value);
			case "name":
				return driver.findElementByName(value);
			case "class":
				return driver.findElementByClassName(value);
			case "xpath":
				return driver.findElementByXPath(value);
			case "link":
				return driver.findElementByLinkText(value);
			case "partialLink":
				return driver.findElementByPartialLinkText(value);
			default:
				break;
			}
			logStep("Found the element using locator as " + locatorType + " with value as " + value, "pass");
		} catch (NoSuchElementException e) {
			logStep("Unable to find the element using locator as " + locatorType + " with value as " + value, "pass");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		try {
			switch (type.toLowerCase()) {
			case "id":
				return driver.findElementsById(value);
			case "name":
				return driver.findElementsByName(value);
			case "class":
				return driver.findElementsByClassName(value);
			case "link":
				return driver.findElementsByLinkText(value);
			case "xpath":
				return driver.findElementsByXPath(value);
			}
			logStep("Found the element using locator as " + type + " with value as " + value, "pass");
		} catch (NoSuchElementException e) {
			logStep("Unable to find element using locator as " + type + " with value as " + value, "fail");
			throw new RuntimeException();
		}
		return null;
	}

	@Override
	public void switchToAlert() {
		try {
			driver.switchTo().alert();
			logStep("Switched to alert", "pass");
		} catch (NoAlertPresentException e) {
			logStep("Unable to switch to alert", "fail");
		}
	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			logStep("Accepted the alert", "pass");
		} catch (NoAlertPresentException e) {
			logStep("Unable to accept the alert", "fail");
		}
	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			logStep("Dismissed the alert", "pass");
		} catch (NoAlertPresentException e) {
			logStep("Dismissed the alert", "fail");
		}
	}

	@Override
	public String getAlertText() {
		try {
			driver.switchTo().alert().getText();
			logStep("Got text from alert", "pass");
		} catch (NoAlertPresentException e) {
			logStep("Unable to get text from alert", "fail");
		}
		return null;
	}

	@Override
	public void typeAlert(String data) {
		try {
			driver.switchTo().alert().sendKeys(data);
			logStep("Able to send text to alert", "pass");
		} catch (NoAlertPresentException e) {
			logStep("Unable to send text to alert", "fail");
		}

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> windowsSet = driver.getWindowHandles();
			List<String> windowsList = new ArrayList<>();
			windowsList.addAll(windowsSet);
			driver.switchTo().window(windowsList.get(index));
			takeSnap();
			logStep("Able to switch to window using index", "pass");
		} catch (NoSuchWindowException e) {
			logStep("Unable to switch to window using index", "fail");
		}

	}

	@Override
	public void switchToWindow(String title) {
		try {
			Set<String> windowsSet = driver.getWindowHandles();
			List<String> windowsList = new ArrayList<>();
			windowsList.addAll(windowsSet);
			for (String eachWindow : windowsList) {
				driver.switchTo().window(eachWindow);
				takeSnap();
				if (driver.getTitle().equalsIgnoreCase(title)) {
					logStep("Window" + eachWindow + " title matches with the given" + title, "pass");
				} else
					logStep("Window" + eachWindow + " title doesn't matches with the given" + title, "pass");
			}
		} catch (NoSuchWindowException e) {
			logStep("Unable to switch to window using index", "fail");
		}

	}

	@Override
	public void switchToFrame(int index) {
		try {
			driver.switchTo().frame(index);
			takeSnap();
			logStep("Able to switch to frame using index", "pass");
		} catch (NoSuchFrameException e) {
			logStep("unable to switch to frame using index", "fail");
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			takeSnap();
			logStep("Able to switch to window using webelement", "pass");
		} catch (NoSuchFrameException e) {
			logStep("Unable to switch to window using webelement", "fail");
		}

	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
			driver.switchTo().frame(idOrName);
			takeSnap();
			logStep("Able to switch to window using Id or Name", "pass");
		} catch (NoSuchFrameException e) {
			logStep("Unable to switch to window using Id or Name", "fail");
		}
	}

	@Override
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
			takeSnap();
			logStep("Able to switch default contents", "pass");
		} catch (NoSuchFrameException e) {
			logStep("Unable to switch default contents", "pass");
		}

	}

	@Override
	public boolean verifyUrl(String url) {
		try {
			if (driver.getCurrentUrl().equalsIgnoreCase(url)) {
				logStep("Current URL matches with the given URL" + url, "pass");
				return true;
			} else
				logStep("Current URL matches with the given URL" + url, "fail");
		} catch (WebDriverException e) {
			System.out.println("Unknown error occured while veirfying the URL");
		}
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		try {
			if (driver.getTitle().equalsIgnoreCase(title)) {
				logStep("Current title matches with the given URL" + title, "pass");
				return true;
			} else
				logStep("Current title matches with the given URL" + title, "fail");
		} catch (WebDriverException e) {
			logStep("Unknown error occured while veirfying the title", "fail");
		}
		return false;
	}

	@Override
	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File des = new File("./snaps/img" + i + ".png");
			FileUtils.copyFile(src, des);
			logStep("Snapshot is taken", "pass");
		} catch (IOException e) {
			logStep("Snapshot is taken", "fail");
		}
		i++;
	}

	@Override
	public void close() {
		try {
			driver.close();
			logStep("Unknown error occured while closing the browser", "pass");
		} catch (WebDriverException e) {
			logStep("Unknown error occured while closing the browser", "fail");
		}

	}

	@Override
	public void quit() {
		try {
			driver.quit();
			logStep("All the browsers are closed", "pass");
		} catch (WebDriverException e) {
			logStep("All the browsers are closed", "fail");
		}
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			logStep("The element " + ele + " clicked successfully", "pass");
		} catch (WebDriverException e) {
			logStep("Unable to click " + ele, "fail");
		} finally {
			takeSnap();
		}

	}

	public void clickwithNoSnap(WebElement ele) {
		try {
			if (ele.isEnabled()) {
				ele.click();
				logStep("The element " + ele + " clicked successfully", "pass");
			} else
				logStep("Unable to click " + ele, "fail");
		} catch (WebDriverException e) {
			logStep("Unable to click " + ele, "fail");
		} finally {
			takeSnap();
		}

	}

	@Override
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			takeSnap();
			logStep("Cleared the text in " + ele, "pass");
		} catch (WebDriverException e) {
			logStep("Unable to clear the text in " + ele, "fail");
		}

	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			System.out.println("The data " + data + " entered successfully");
			takeSnap();
			logStep("Cleared and typed the text in " + ele, "pass");
		} catch (WebDriverException e) {
			logStep("Unable to clear and type the text in " + ele, "fail");
		}

	}

	@Override
	public String getElementText(WebElement ele) {
		try {
			String text = ele.getText();
			logStep("Get element by text for " + ele, "pass");
			return text;
		} catch (NoSuchElementException e) {
			logStep("No such element exception " + ele, "fail");
		}
		return null;

	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select selectSource = new Select(ele);
			selectSource.selectByVisibleText(value);
			logStep("Selected dropdown value using text for" + ele + "and value as " + value, "pass");
		} catch (NoSuchElementException e) {
			logStep("No such element exception " + ele, "fail");
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select selectSource = new Select(ele);
			List<WebElement> options = selectSource.getOptions();
			selectSource.selectByIndex(options.size() - 1);
			logStep("Selected dropdown value using index for" + ele + "and value as " + index, "pass");
		} catch (NoSuchElementException e) {
			logStep("No such element exception " + ele, "fail");
		}

	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			Select selectSource = new Select(ele);
			selectSource.selectByValue(value);
			logStep("Selected dropdown value using value for" + ele + "and value as " + value, "pass");
		} catch (NoSuchElementException e) {
			logStep("No such element exception " + ele, "fail");
		}

	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		try {
			if (ele.getText().equals(expectedText)) {
				logStep("Given text " + expectedText + " matches with the text in " + ele, "pass");
				return true;
			} else
				logStep("Given text " + expectedText + " doesn't match with the text in " + ele, "fail");
		} catch (NoSuchElementException e) {
			logStep("No such element exception " + ele, "fail");
		}
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		try {
			if (ele.getText().contains(expectedText)) {
				logStep("Given text " + expectedText + " contains with the text in " + ele, "pass");
				return true;
			} else
				logStep("Given text " + expectedText + " doesn't contains with the text in " + ele, "fail");
		} catch (NoSuchElementException e) {
			logStep("No such element exception " + ele, "fail");
		}

		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		try {
			if (ele.isDisplayed()) {
				logStep(ele + " is not selected", "pass");
				return true;
			} else
				logStep(ele + " is not selected", "fail");
		} catch (NoSuchElementException e) {
			logStep("No such element exception " + ele, "fail");
		}
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		try {
			if (ele.isDisplayed()) {
				logStep(ele + " is disappeared", "pass");
				return true;
			} else
				logStep(ele + " is not disappeared", "fail");
		} catch (NoSuchElementException e) {
			logStep("No such element exception " + ele, "fail");
		}
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		try {
			if (ele.isEnabled()) {
				logStep(ele + " is enabled", "pass");
				return true;
			} else
				logStep(ele + " is not enabled", "fail");
		} catch (NoSuchElementException e) {
			logStep("No such element exception " + ele, "fail");
		}
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		try {
			if (ele.isSelected()) {
				logStep(ele + " is selected", "pass");
				return true;
			} else
				logStep(ele + " is not selected", "fail");
		} catch (NoSuchElementException e) {
			logStep("No such element exception " + ele, "fail");
		}
		return false;
	}

}
