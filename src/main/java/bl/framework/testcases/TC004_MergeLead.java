package bl.framework.testcases;

import java.util.*;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.commonMethods.ProjectMethods;

public class TC004_MergeLead extends ProjectMethods{
     
	@BeforeTest(groups= {"common"})
	public void setData() {
		tcName = "TC004_MergeLead";
		tcDescription = "Merge Leads in leaftaps";
		tcAuthor = "vignesh";
		tcCategory = "Smoke";
	} 
	
	
	@Test 
	//@Test(groups= {"reg"})
	public void merge() {		
		WebElement leadsMenuButton = locateElement("link", "Leads");
		click(leadsMenuButton);

		click(locateElement("link", "Merge Leads"));
		
		click(locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]"));
		switchToWindow(1);
		
		clearAndType(locateElement("name", "id"), "10000");
		
		click(locateElement("name", "x-btn-text"));
		click(locateElement("name", "linktext"));
		
		defaultContent();
		
		click(locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]"));
		switchToWindow(2);
		clearAndType(locateElement("name", "id"), "10000");
		
		click(locateElement("name", "x-btn-text"));
		click(locateElement("name", "linktext"));
		defaultContent();
		
		click(locateElement("name", "buttonDangerous"));
		
		acceptAlert();
		
		click(locateElement("xpath", "//a[text()='Find Leads']"));
		close();
	
	}
	
}
