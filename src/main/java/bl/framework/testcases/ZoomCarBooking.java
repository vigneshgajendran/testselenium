package bl.framework.testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.commonMethods.ProjectMethods;

public class ZoomCarBooking extends ProjectMethods {

	@BeforeTest
	public void setData() {
		tcName = "TC005_ZoomCarBooking";
		tcDescription = "ZoomCarBooking";
		tcAuthor = "vignesh";
		tcCategory = "Smoke";
	}

	@Test
	public void bookCar() {
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		click(locateElement("xpath", "//a[@title='Start your wonderful journey']"));
		WebElement locations = locateElement("class", "component-popular-locations");
		List<WebElement> location = locations.findElements(By.tagName("div"));
		click(location.get(1));
		click(locateElement("class", "proceed"));

		// Get the current date

		Date date = new Date();

		// Get only the date (and not month, year, time etc)

		DateFormat sdf = new SimpleDateFormat("dd");

		// Get today's date

		String today = sdf.format(date);

		// Convert to integer and add 1 to it

		int tomorrow = Integer.parseInt(today) + 1;

		// Print tomorrow's date

		click(locateElement("xpath", "//div[contains(text(),'" + tomorrow + "')]"));

		click(locateElement("class", "proceed"));
		click(locateElement("class", "proceed"));
		List<String> price = new ArrayList<>();
		List<WebElement> listofprice = locateElements("xpath", "//div[@class='price']");
		for (WebElement eachprice : listofprice) {
			price.add(eachprice.getText().replaceAll("\\D", ""));
		}

		String max = Collections.max(price);

		System.out.println(max);

		int carMaxIndex = price.indexOf((Integer.parseInt(max)) + 1);

		click(locateElement("xpath", "//div[contains(text(),'"+max+"')]/../button[@name='book-now']"));
		//driver.findElement(By.xpath("//div[contains(text(),'329')]/../button[@name='book-now']")).click();
		
		
	
	}

}
