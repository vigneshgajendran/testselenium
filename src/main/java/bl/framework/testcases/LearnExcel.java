package bl.framework.testcases;

import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	public static void main(String[] args) throws IOException {
		// Enter the Excel Path
		XSSFWorkbook wbook = new XSSFWorkbook("./data/TC002.xlsx");
		// Enter the Sheet Name
		XSSFSheet sheet = wbook.getSheet("Sheet1");
		// Get the rows count
		int lastRowNum = sheet.getLastRowNum();
		// Get the columns count
		short lastCellNum = sheet.getRow(0).getLastCellNum();

		for (int i = 1; i <= lastRowNum; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < lastCellNum; j++) {
				XSSFCell cell = row.getCell(j);
				String stringCellValue = cell.getStringCellValue();
				//System.out.println(stringCellValue);

				CellType cellType = cell.getCellType();

				if (cellType.STRING != null)
					System.out.println(cell.getStringCellValue());
				else if (cellType.NUMERIC != null) {
					if (DateUtil.isCellDateFormatted(cell))
						System.out.println(cell.getDateCellValue());
					else
						System.out.println(cell.getNumericCellValue());
				} else if (cellType.BOOLEAN != null)
					System.out.println(cell.getBooleanCellValue());
				else if (cellType._NONE != null)
					System.out.println("null");
				else if (cellType.ERROR != null)
					System.out.println(cell.getErrorCellValue());
				else if (cellType.FORMULA != null)
					System.out.println(cell.getCellFormula());
			}
		}
		wbook.close();
	}

}
