package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.commonMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods {

	@BeforeTest(groups= {"common"})
	public void setData() {
		tcName = "TC002_EditLead";
		tcDescription = "Edit Lead in leaftaps";
		tcAuthor = "vignesh";
		tcCategory = "Smoke";
	} 
	
	
	//@Test(dependsOnMethods= "bl.framework.testcases.TC001_LoginAndLogout.crtLead") 
	//@Test(groups= {"smoke"})
	@Test
	public void edit() {		
		WebElement leadsMenuButton = locateElement("link", "Leads");
		click(leadsMenuButton);

		WebElement findLeadsMenuButton = locateElement("xpath", "//a[text()='Find Leads']");
		click(findLeadsMenuButton);

		clearAndType(locateElement("xpath", "//div[@class='x-form-item x-tab-item']/following::input"), "First Name");

		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("TimeOut Exception");
		}

		WebElement firstCell = locateElement("xpath",
				"(//div[@class=\"x-grid3-cell-inner x-grid3-col-partyId\"]//a[@class=\"linktext\"])[1]");
		click(firstCell);
		System.out.println(driver.getTitle());

		WebElement editButton = locateElement("xpath",
				"//div[@class='frameSectionExtra']//preceding-sibling::a[text()='Edit']");
		click(editButton);
		clearAndType(locateElement("id", "updateLeadForm_companyName"), "New Company");

		WebElement submitButton = locateElement("name", "submitButton");
		click(submitButton);

		String newCompanyID = driver.findElementById("viewLead_companyName_sp").getText();
		if (newCompanyID.contains("New Company"))
			System.out.println("New company contact is updated");
		driver.close();

	}

}
