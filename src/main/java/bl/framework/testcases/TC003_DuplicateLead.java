package bl.framework.testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.commonMethods.ProjectMethods;

public class TC003_DuplicateLead extends ProjectMethods {
	
	@BeforeTest(groups= {"common"})
	public void setData() {
		tcName = "TC003_DuplicateLead";
		tcDescription = "Duplicate Lead in leaftaps";
		tcAuthor = "vignesh";
		tcCategory = "Smoke";
	} 
	
	@Test
	//@Test(groups= {"sanity"})
	public void duplicate() {		
		WebElement leadsMenuButton = locateElement("link", "Leads");
		click(leadsMenuButton);

		WebElement findLeadsMenuButton = locateElement("xpath", "//a[text()='Find Leads']");
		click(findLeadsMenuButton);

		// Edit Tab
		click(locateElement("xpath", "(//span[@class='x-tab-strip-inner'])[3]"));
		// enter email addr - testgts@test.com
		clearAndType(locateElement("xpath", "//input[@name='emailAddress']"), "testgts@test.com");

		click(locateElement("xpath", "(//button[@class='x-btn-text'])[7]"));

		String duplicateName = getElementText(
				locateElement("xpath", "//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']/div/a"));
		click(locateElement("xpath", "//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']/div/a"));
		// Click Duplicate Lead
		click(locateElement("xpath", "//a[@class='subMenuButton']"));

		verifyUrl("Duplicate Lead");

		click(locateElement("class", "smallSubmit"));

		String DuplicateCompanyText = locateElement("id", "viewLead_companyName_sp").getText();

		if (DuplicateCompanyText.equals(duplicateName))
			System.out.println("Confirmed");
		else
			System.out.println("Duplicate name is not correct");

		close();

	}

}
