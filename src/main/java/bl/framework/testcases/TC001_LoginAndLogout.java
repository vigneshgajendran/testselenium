package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.commonMethods.ProjectMethods;

public class TC001_LoginAndLogout extends ProjectMethods {

	@BeforeTest(groups = { "common" })
	public void setData() {
		tcName = "TC001_CreateLead";
		tcDescription = "Create a new Lead in leaftaps";
		tcAuthor = "vignesh";
		tcCategory = "Smoke";
		datasheetsource="TC002";
	}

	// @Test(invocationCount=2,invocationTimeOut=80000)
	// @Test(groups= {"smoke"})
	@Test(dataProvider= "getExcelData")
	public void crtLead(String cname,String fName,String lName) {
		WebElement createLeadButton = locateElement("link", "Create Lead");
		click(createLeadButton);
		WebElement companyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(companyName, cname);

		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(firstName, fName);

		WebElement lastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(lastName, lName);

		WebElement companySelect = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(companySelect, "Employee");

		WebElement marketingCampaingnSelect = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingValue(marketingCampaingnSelect, "CATRQ_CARNDRIVER");

		WebElement localFirstName = locateElement("id", "createLeadForm_firstNameLocal");
		clearAndType(localFirstName, "Local First Name");

		WebElement localLastName = locateElement("id", "createLeadForm_lastNameLocal");
		clearAndType(localLastName, "Local Last Name");

		WebElement salutation = locateElement("id", "createLeadForm_personalTitle");
		clearAndType(salutation, "MR");

		WebElement title = locateElement("id", "createLeadForm_generalProfTitle");
		clearAndType(title, "Title");

		WebElement departmentName = locateElement("id", "createLeadForm_departmentName");
		clearAndType(departmentName, "Department");

		WebElement annualIncome = locateElement("id", "createLeadForm_annualRevenue");
		clearAndType(annualIncome, "100000");

		WebElement currencySelect = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingIndex(currencySelect, 5);

		// Select Industry
		selectDropDownUsingIndex(locateElement("id", "createLeadForm_industryEnumId"), 5);

		WebElement noOfEmp = locateElement("id", "createLeadForm_numberEmployees");
		clearAndType(noOfEmp, "5");

		WebElement ownership = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(ownership, "Partnership");

		WebElement sicCode = locateElement("id", "createLeadForm_sicCode");
		clearAndType(sicCode, "12345");

		WebElement tickerSymbol = locateElement("id", "createLeadForm_tickerSymbol");
		clearAndType(tickerSymbol, "Ticker Symbol");

		WebElement description = locateElement("id", "createLeadForm_description");
		clearAndType(description, "Description");

		WebElement importantNote = locateElement("id", "createLeadForm_importantNote");
		clearAndType(importantNote, "Important Note");

		// Contact Information
		WebElement primaryPhoneAreaCode = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		clearAndType(primaryPhoneAreaCode, "Primary Phone Area Code");

		WebElement createLeadForm_primaryPhoneNumber = locateElement("id", "createLeadForm_primaryPhoneNumber");
		clearAndType(createLeadForm_primaryPhoneNumber, "Create LeadForm Primary PhoneNumber");

		WebElement createLeadForm_primaryPhoneExtension = locateElement("id", "createLeadForm_primaryPhoneExtension");
		clearAndType(createLeadForm_primaryPhoneExtension, "Create LeadForm Primary Phone Extension");

		WebElement createLeadForm_primaryPhoneAskForName = locateElement("id", "createLeadForm_primaryPhoneAskForName");
		clearAndType(createLeadForm_primaryPhoneAskForName, "Create LeadForm Primary Phone Ask For Name");

		WebElement createLeadForm_primaryEmail = locateElement("id", "createLeadForm_primaryEmail");
		clearAndType(createLeadForm_primaryEmail, "aaa@gmail.com");

		WebElement createLeadForm_primaryWebUrl = locateElement("id", "createLeadForm_primaryWebUrl");
		clearAndType(createLeadForm_primaryWebUrl, "Create LeadForm Primary Web Url");

		// Primary Address
		WebElement createLeadForm_generalToName = locateElement("id", "createLeadForm_generalToName");
		clearAndType(createLeadForm_generalToName, "Create LeadForm General To Name");

		WebElement createLeadForm_generalAttnName = locateElement("id", "createLeadForm_generalAttnName");
		clearAndType(createLeadForm_generalAttnName, "create LeadForm General Attn Name");

		WebElement createLeadForm_generalAddress1 = locateElement("id", "createLeadForm_generalAddress1");
		clearAndType(createLeadForm_generalAddress1, "createLeadForm_generalAddress1");

		WebElement createLeadForm_generalAddress2 = locateElement("id", "createLeadForm_generalAddress2");
		clearAndType(createLeadForm_generalAddress2, "createLeadForm_generalAddress2");

		WebElement createLeadForm_generalCity = locateElement("id", "createLeadForm_generalCity");
		clearAndType(createLeadForm_generalCity, "createLeadForm_generalCity");

		WebElement countryGeoId = locateElement("id", "createLeadForm_generalCountryGeoId");
		selectDropDownUsingValue(countryGeoId, "IND");

		WebElement stateGeoId = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(stateGeoId, "TAMILNADU");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement createLeadForm_generalPostalCode = locateElement("id", "createLeadForm_generalPostalCode");
		clearAndType(createLeadForm_generalPostalCode, "999");

		WebElement createLeadForm_generalPostalCodeExt = locateElement("id", "createLeadForm_generalPostalCodeExt");
		clearAndType(createLeadForm_generalPostalCodeExt, "999");

		click(locateElement("name", "submitButton"));

		String companyID = locateElement("id", "viewLead_companyName_sp").getText();
	}
    
	@DataProvider(name="fetchData")
	public String[][] getData() {
		String[][] data = new String[2][3];
		data[0][0] = "TestLeaf";
		data[0][1] = "Vignesh";
		data[0][2] = "G";
		
		data[1][0] = "TestLeaf";
		data[1][1] = "Vims";
		data[1][2] = "C";
		return data;
	}	
}
