package bl.framework.testcases;

import java.util.List;

import org.etsi.uri.x01903.v13.impl.GenericTimeStampTypeImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.commonMethods.ProjectMethods;

public class Facebook extends ProjectMethods {

	@BeforeTest
	public void setData() {
		tcName = "TC006_fb";
		tcDescription = "fb";
		tcAuthor = "vignesh";
		tcCategory = "Smoke";
	}

	@Test
	public void fb() {
		startApp("chrome", "https://www.facebook.com");
		clearAndType((locateElement("xpath", "//input[@name='email']")), "");
		clearAndType((locateElement("xpath", "//input[@name='pass']")), "");
		click(locateElement("xpath", "//input[@data-testid='royal_login_button']"));
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		clearAndType((locateElement("xpath", "//input[@data-testid='search_input']")), "Testleaf");
		click(locateElement("xpath", "//button[@aria-label='Search']"));
		WebDriverWait wait = new WebDriverWait(driver, 30);

		WebElement testLeafResults = driver.findElementByXPath("//div[@id='BrowseResultsContainer']");
		// System.out.println(testLeafResults.getText());
		List<WebElement> likeButton = testLeafResults.findElements(
				By.xpath("//div[@data-testid='browse-result-content']//button[@data-bt='{\"ct\":\"like_page\"}']"));
		List<WebElement> resultLink = testLeafResults.findElements(
				By.xpath("//div[@data-testid='browse-result-content']//a[@data-testid='browse-result-link']"));
		for (WebElement eachlike : likeButton) {
			for (WebElement eachLink : resultLink) {
				wait.until(ExpectedConditions.elementToBeClickable(eachlike));
				if (eachlike.getText().equalsIgnoreCase("like")) {
					click(eachlike);
					click(eachLink);
				} else if (eachlike.getText().equalsIgnoreCase("liked"))
					System.out.println("is already liked");
			}
		}
		// div[@data-testid='browse-result-content']//a[@data-testid='browse-result-link']

//		for (WebElement eachResult : testLeafResults) {
//			if (eachResult.getText().equals("Testleaf")) {				
//				if (locateElement("xpath",
//						"(//div[@data-testid='browse-result-content']//button[@data-bt='{\"ct\":\"like_page\"}'])[" + i
//								+ "]").getText().equals("Like"))
//					click(locateElement("xpath",
//							"(//div[@data-testid='browse-result-content']//button[@data-bt='{\"ct\":\"like_page\"}'])["
//									+ i + "]"));
//				else if (locateElement("xpath",
//						"(//div[@data-testid='browse-result-content']//button[@data-bt='{\"ct\":\"like_page\"}'])[" + i
//								+ "]").getText().equals("Liked"))
//					System.out.println("It is already liked");
//				verifyTitle("Testleaf");
//
//				click(locateElement("xpath", "(//div[@data-testid='browse-result-content']//button)[" + i + "]"));
//				i++;
//			}
	}
}
